"""This profile instantiates a d840 machine connected to a Skylark FAROS massive MIMO system (64-antenna), comprised of FAROS hub and its connected Iris radio chains (four chains with four and two chains with eight Irises) as well as two Iris clients. The PC boots with Ubuntu 16.04 and includes a MATLAB installation that could be used to run experiments on FAROS with RENEWLab demos. For more information on RENEWLab, see [RENEW documentation page](https://docs.renew-wireless.org)

Instructions:
The FAROS hub and PC are connected via a private 10Gbps link. All Iris radios and the FAROS hub should come up with address between 192.168.1.101 and 192.168.1.200. These addresses are reachable by first logging in to the PC. For more information on how to start an experiment with massive MIMO equipment on POWDER, see [this page](https://docs.renew-wireless.org/getting-started/powder/). 

To get started with FAROS massive MIMO hardware, see [RENEW Hardware Documentation](https://docs.renew-wireless.org/getting-started/hardware/).

For questions about access to the required PC type and massive MIMO radio devices, please contact support@powderwireless.net

A step-by-step procedure to run simple demos is as follows:

- Once your experiment is ready, from your terminal, ssh to pc1 with X11 forwarding:
 
	`ssh -X USERNAME@pc19-meb.emulab.net`

- Go to the renew-software repository python demos folder: 

	`cd /local/repository/renew-software/PYTHON/DEMOS`

- Run example 1 (bursty oscilloscope and spectrum analyzer demo): 

	`sudo python3 SISO_RX.py --serial RF3E000143`

- Run example 2 (sending and receiving a sinewave burst between two Iris devices in tdd mode): 

	`python3 SISO_TXRX_TDD.py`

- In case you get the following error after running any of the examples, try re-running the code. This is a transient error, related to the discovery of Iris devices.

	`RuntimeError: SoapySDR::Device::make() no driver specified and no enumeration results`


To learn more about the rest of demos and examples and how to run your own apps, see [RENEWLab documentation](https://docs.renew-wireless.org/dev-suite/design-flows/)
"""

import geni.portal as portal
import geni.urn as urn
import geni.rspec.pg as pg
import geni.rspec.emulab as elab
import geni.rspec.emulab.spectrum as spectrum

# Resource strings
PCIMG = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU18-64-STD"
MATLAB_DS_URN = "urn:publicid:IDN+emulab.net:powderprofiles+ltdataset+matlab-extra"
MATLAB_MP = "/usr/local/MATLAB"
STARTUP_SCRIPT = "/local/repository/faros_start.sh"
PCHWTYPE = "d840"
FAROSHWTYPE = "faros_sfp"
IRISHWTYPE = "iris030"
SHVLAN_NAME = "iris-sharedlan"
DEF_BS_NAME = 'mmimo-ac'
DEF_BS_SIZE = 0
DEF_BS_MOUNT_POINT = "/opt/data"
DEF_REMDS_MP = "/opt/data"

REMDS_TYPES = [("readwrite", "Read-Write (persistent)"),
               ("readonly", "Read Only"),
               ("rwclone", "Read-Write Clone (not persistent)")]

MMIMO_ARRAYS = ["", "mmimo-ac", "mmimo1-meb", "mmimo1-honors"]

LAB_CLIENTS = ["", "iris03", "iris04", "irisclients-ac", "irisclients2-meb", "iris1-meb3555", "iris2-meb3555"]

#
# Profile parameters.
#
pc = portal.Context()

# Frequency/spectrum parameters
pc.defineStructParameter(
    "freq_ranges", "Range", [],
    multiValue=True,
    min=1,
    multiValueTitle="Frequency ranges for over-the-air operation.",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Min",
            portal.ParameterType.BANDWIDTH,
            2496.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Max",
            portal.ParameterType.BANDWIDTH,
            2506.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ])

pc.defineStructParameter(
    "mmimo_devices", "mMIMO Devices", [],
    multiValue=True,
    min=0,
    multiValueTitle="Massive MIMO devices to allocate.",
    members=[
        portal.Parameter(
            "mmimoid", "ID of Massive MIMO array to allocate.",
            portal.ParameterType.STRING, MMIMO_ARRAYS[0], MMIMO_ARRAYS
        ),
    ])

pc.defineStructParameter(
    "lab_clients", "Lab Iris Clients", [],
    multiValue=True,
    min=0,
    multiValueTitle="Iris Lab clients to allocate.",
    members=[
        portal.Parameter(
            "irisid", "ID of iris client to allocate.",
            portal.ParameterType.STRING, LAB_CLIENTS[0], LAB_CLIENTS
        ),
    ])

pc.defineParameter("roofclients", "Allocate rooftop Iris client radios.",
                   portal.ParameterType.BOOLEAN, False)

pc.defineParameter("matlabds", "Attach the Matlab dataset to the compute host.",
                   portal.ParameterType.BOOLEAN, True)

pc.defineParameter("hubints", "Number of interfaces to attach on hub (def: 2)",
                   portal.ParameterType.INTEGER, 2,
                   longDescription="This can be a number between 1 and 4.",
                   advanced=True)

pc.defineParameter("fixedid", "Fixed PC Node_id (Optional)",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Fix 'pc1' to this specific node.  Leave blank to allow for any available node of the correct type.")

pc.defineParameter("remds", "Remote Dataset (Optional)",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Insert URN of a remote dataset to mount. Leave blank for no dataset.")

pc.defineParameter("remmp", "Remote Dataset Mount Point (Optional)",
                   portal.ParameterType.STRING, DEF_REMDS_MP, advanced=True,
                   longDescription="Mount point for optional remote dataset.  Ignored if the 'Remote Dataset' parameter is not set.")

pc.defineParameter("remtype", "Remote Dataset Mount Type (Optional)",
                   portal.ParameterType.STRING, REMDS_TYPES[0], REMDS_TYPES,
                   advanced=True, longDescription="Type of mount for remote dataset.  Ignored if the 'Remote Dataset' parameter is not set.")

# Bind and verify parameters.
params = pc.bindParameters()

for i, frange in enumerate(params.freq_ranges):
    if frange.freq_max - frange.freq_min < 1:
        perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ["freq_ranges[%d].freq_min" % i, "freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)

if params.hubints < 1 or params.hubints > 4:
    perr = portal.ParameterError("Number of interfaces on hub to connect must be between 1 and 4 (inclusive).")
    portal.context.reportError(perr)

pc.verifyParameters()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Mount a remote dataset
def connect_DS(node, urn, mp, dsname = "", dstype = "rwclone"):
    if not dsname:
        dsname = "ds-%s" % node.name
    bs = request.RemoteBlockstore(dsname, mp)
    if dstype == "rwclone":
        bs.rwclone = True
    elif dstype == "readonly":
        bs.readonly = True
        
    # Set dataset URN
    bs.dataset = urn

    # Create link from node to OAI dataset rw clone
    bslink = request.Link("link_%s" % dsname, members=(node, bs.interface))
    bslink.vlan_tagging = True
    bslink.best_effort = True

# Request a PC
pc1 = request.RawPC("pc1")
if params.fixedid:
    pc1.component_id=params.fixedid
else:
    pc1.hardware_type = PCHWTYPE
pc1.disk_image = PCIMG
if params.matlabds:
    connect_DS(pc1, MATLAB_DS_URN, MATLAB_MP, "matlab1")
pc1.addService(pg.Execute(shell="sh", command=STARTUP_SCRIPT))
if1pc1 = pc1.addInterface("if1pc1", pg.IPv4Address("192.168.1.1", "255.255.255.0"))
#if1pc1.bandwidth = 40 * 1000 * 1000 # 40 Gbps
bs2 = pc1.Blockstore("scratch","/scratch")
bs2.placement = "nonsysvol"
if params.remds:
    connect_DS(pc1, params.remds, params.remmp, dstype=params.remtype)

# LAN connecting up everything (if needed).  Members are added below.
lan1 = None
if len(params.mmimo_devices) or len(params.lab_clients) or params.roofclients:
    lan1 = request.LAN("lan1")
    lan1.vlan_tagging = False
    lan1.setNoBandwidthShaping()
    lan1.addInterface(if1pc1)

# Request all Faros BSes requested
for i, mmimodev in enumerate(params.mmimo_devices):
    mm = request.RawPC("mm%d" % i)
    mm.component_id = mmimodev.mmimoid
    mm.hardware_type = FAROSHWTYPE
    for j in range(params.hubints):
        mmif = mm.addInterface()
        lan1.addInterface(mmif)

# Request rooftop Iris clients.
if params.roofclients:
    # Allocate USTAR Iris
    ir1 = request.RawPC("ir1")
    ir1.hardware_type = IRISHWTYPE
    ir1.component_id = "iris1-ustar"
    # Enable connectivity to shared vlan with rooftop Irises
    lan1.connectSharedVlan(SHVLAN_NAME)

for i, irisdev in enumerate(params.lab_clients):
    labir = request.RawPC("ir%d" % i)
    labir.hardware_type = IRISHWTYPE
    labir.component_id = irisdev.irisid
    labif = labir.addInterface()
    lan1.addInterface(labif)

# Add frequency request(s)
for frange in params.freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, 100)

# Print the RSpec to the enclosing page.
pc.printRequestRSpec()
